# apt commands
alias update='sudo apt update'
alias upgrade='sudo apt update && sudo apt upgrade && sudo apt install -f && sudo apt autoremove'
alias autorm='sudo apt-get autoremove'
alias purge='sudo apt-get purge'
alias install='sudo apt install'

# File-related commands
alias la='ls -lah'
alias lh='ls -ld .?*'
alias cp='cp -Rv'
alias rm='rm -rvI'
alias mv='mv -v'
alias du='du -B M -d 1 | sort -nr'
alias chmod='chmod -v'
alias chown='chown -v'
alias grep='grep --color=always -i'

# System related commands
alias free='free -h'
alias myip='curl icanhazip.com'
alias mount='mount | column -t'
alias df='df -h'

# Specific files
alias nanoalias='nano ~/.bash_aliases'
alias nanofstab='sudo nano /etc/fstab'
alias cdd='cd ~/Downloads'

# Screen
alias tv-screen='xrandr --output HDMI1 --transform 1.04,0,-45,0,1.04,-27,0,0,1 && pkill redshift'
alias laptop-screen='xrandr --output eDP1 --transform 1,0,0,0,1,0,0,0,1 && xrandr --output VGA1 --transform 1,0,0,0,1,0,0,0,1 && redshift -l 48.36:14.52 -t 7500:3000'

# Gradle
alias assembleDebug='./gradlew assembleDebug; alert'
alias assembleQa='./gradlew assembleQa; alert'
alias assembleRelease='./gradlew assembleRelease; alert'
alias testAll='./gradlew test connectedAndroidTest; alert'
alias alert='paplay ~/scripts/eventually.ogg'
